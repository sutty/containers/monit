#!/bin/sh

# Registers the local IPv6 address to the DNS

IP6="`whatsmyip6`"
KNOT="${KNOT:-${IP6}}"

test -z "${IP6}" && exit 1
test -z "${KNOT}" && exit 1
test -z "${DOMAIN}" && exit 1

# Add a dot if the hostname contains the domain
echo "${HOSTNAME}" | grep -q "\.${DOMAIN}$" && DOT="."

knsupdate <<DNS
server ${KNOT}
zone ${DOMAIN}.
origin ${DOMAIN}.
ttl 60
add ${HOSTNAME}${DOT} AAAA ${IP6}
send
quit
DNS

exit $?
