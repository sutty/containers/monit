FROM alpine:3.13.6 AS build
ENV EMAIL monit@sutty.nl

RUN apk add --no-cache tzdata

COPY ./monitrc /etc/monitrc
RUN chmod 600 /etc/monitrc
RUN sed -re "s/@@EMAIL@@/${EMAIL}/" -i /etc/monitrc

FROM alpine:3.13.6
MAINTAINER "f <f@sutty.nl>"

# TODO: We don't want to receive lots of email, so we'll change this to
# a pingy instance that will also work as a status site.
ENV EMAIL monit@sutty.nl

# Sutty's repository
RUN echo https://alpine.sutty.nl/alpine/v3.13/sutty >> /etc/apk/repositories
RUN wget https://alpine.sutty.nl/alpine/sutty.pub -O /etc/apk/keys/alpine@sutty.nl-5ea884cd.rsa.pub

# Install monit and remove default config
RUN apk add --no-cache tini monit knsupdate
COPY --from=build /usr/share/zoneinfo/UTC /etc/localtime

# Create directories
RUN install -dm 2750 -o root -g root /etc/monit.d
RUN install -dm 2750 -o root -g root /var/lib/monit

# Install config
COPY --from=build /etc/monitrc /etc/monitrc
COPY ./nsupdate.sh /usr/local/bin/nsupdate
COPY ./whatsmyip6.sh /usr/local/bin/whatsmyip6

# Allow access to the web GUI
EXPOSE 2812

# Use tini as init
ENTRYPOINT ["/sbin/tini", "--"]

# Run monit, it will take care of running services afterwards
CMD ["/usr/bin/monit"]
