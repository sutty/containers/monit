#!/bin/sh

ip address show ${1:-eth0} | grep inet6 | grep global | tr -s " "  | cut -d " " -f 3 | cut -d / -f 1
